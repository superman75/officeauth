import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MSAdal, AuthenticationContext } from '@ionic-native/ms-adal';
import { AuthServiceProvider} from "../../providers/auth-service/auth-service";

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  responseData:any [];
  accessToken: any;
  showResult: string;
  test:any;
  hasNoData: boolean = false;
  constructor(public navCtrl: NavController, public authService: AuthServiceProvider, private msAdal: MSAdal) {
    this.accessToken = localStorage.AppaccessToken;
    console.log(localStorage.AppaccessToken);
    this.authService.getUserProfile(this.accessToken).then((result:any) => {
      this.responseData=result.value;
    },(error)=>{
      this.showResult = "fail" + JSON.stringify(error);
    }).catch((err) => { console.log(err); this.showResult = "error" + JSON.stringify(err);});
  }

  logout(){
    localStorage.clear();
    let authContext: AuthenticationContext = this.msAdal.createAuthenticationContext('https://login.microsoftonline.com/common');
    authContext.tokenCache.clear();
    this.navCtrl.setRoot('LoginPage');
  }

}
