import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Configurations} from "./config";
import { MSAdal, AuthenticationContext, AuthenticationResult } from '@ionic-native/ms-adal';
/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthServiceProvider {
  rootPage: any;
  constructor(public http: HttpClient, public msAdal: MSAdal) {
    console.log('Hello AuthServiceProvider Provider');
  }

  login() {
    return new Promise((resolve, reject) => {
      let authContext: AuthenticationContext = this.msAdal.createAuthenticationContext('https://login.microsoftonline.com/common');
      authContext.acquireTokenAsync(Configurations.GRAPH_RESOURCE, Configurations.CLIENT_ID, 'msal82ca0a78-4c9f-4326-9044-5149d61ddf73://auth', '', '')
        .then((authResponse: AuthenticationResult) => {
          localStorage.setItem("AppaccessToken", authResponse.accessToken);
          resolve(authResponse.accessToken);
        })
        .catch((e: any) => reject(console.log('Authentication failed', e)));
    });
  }

  getUserProfile(accessToken) {

    return new Promise((resolve, reject) => {

      this.http.get("https://graph.microsoft.com/v1.0/users", {
        headers: new HttpHeaders().set("Authorization", "Bearer " + accessToken)
      })
        .subscribe((res: any) => {
          resolve(res);
        }, (err) => {
        reject(err);
      })
    });
  }

  getRefreshedToken() {
    return new Promise((resolve, reject) => {
      let authContext: AuthenticationContext = this.msAdal.createAuthenticationContext('https://login.microsoftonline.com/common');
      authContext.acquireTokenSilentAsync(Configurations.GRAPH_RESOURCE, Configurations.CLIENT_ID, '')
        .then((authResponse: AuthenticationResult) => {
          localStorage.setItem("AppaccessToken", authResponse.accessToken);
          resolve(authResponse.accessToken);
        })
        .catch((e: any) => reject(console.log('Authentication failed', e)));

    });

  }
}
