import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AuthServiceProvider} from "../providers/auth-service/auth-service";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  accessToken: boolean;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,  private auth: AuthServiceProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.accessToken = localStorage.AppaccessToken == "undefined" || localStorage.AppaccessToken == null;

    if (this.accessToken) {
      this.rootPage = 'LoginPage';

    }
    else {

      this.auth.getRefreshedToken().then((result)=>{

        this.rootPage='HomePage';
      })
    }
  }
}

